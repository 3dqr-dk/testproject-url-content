﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public static class FileInfoExtension
{
    public static bool IsLocked(this FileInfo file)
    {
        if (file.Exists)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }
        //if the file does not exists, the file is not locked ;)
        return false;
    }
}

public class CertificatePolicy : ICertificatePolicy
{
    bool ICertificatePolicy.CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem)
    {
        return true;
    }
}

public class Download
{
    public enum DownloadState
    {
        Created,
        Started,
        Finished
    }

    protected string downloadURL;
    protected string destinationPath;

    protected WebClient webClient;

    protected DownloadState state;

    #region Constructors    

    public Download(string downloadURL, string destinationPath)
    {
        this.downloadURL = downloadURL;
        this.destinationPath = destinationPath + Path.GetExtension(downloadURL);

        state = DownloadState.Created;

        Debug.Log("Download from " + this.downloadURL + " created.");
    }

    #endregion

    #region Public Methods

    public IEnumerator StartDownloadAsync()
    {
        ServicePointManager.CertificatePolicy = new CertificatePolicy();
        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

        //try to find out, if file is already locked by another process
        FileInfo file = new FileInfo(destinationPath);
        while (file.IsLocked())
        {
            //Debug.Log("Download::Start - Existing file is locked, webclient is waiting for release  " + sourceURL);
            yield return new WaitForSeconds(0.25f);
        }

        //start download per webclient

        webClient = new WebClient();

        webClient.DownloadProgressChanged += DownloadProgressChanged_WebClient;
        webClient.DownloadFileCompleted += DownloadFileCompleted_WebClient;
        webClient.DownloadFileAsync(new System.Uri(downloadURL), destinationPath);

        Debug.Log("Started download from " + downloadURL + " ... ");

        state = DownloadState.Started;
    }

    public void StopDownload()
    {
        if (state == DownloadState.Started)
        {
            webClient.CancelAsync();

            Debug.Log("Stopped download from " + downloadURL + " ... ");

            state = DownloadState.Created;
        }
    }

    public IEnumerator TryRemoveDownloadUntilSuccessful(float secondsUntilTimeout = 60, float waitingIntervalInSeconds = 0.5f)
    {
        bool success = TryRemoveDownload();
        float secondsOfRetrying = 0;

        while (!success)
        {
            if (secondsOfRetrying >= secondsUntilTimeout)
            {
                Debug.Log("Aborted removal of download from " + downloadURL + ".");

                yield break;
            }

            yield return new WaitForSeconds(waitingIntervalInSeconds);

            secondsOfRetrying += waitingIntervalInSeconds;
            success = TryRemoveDownload();
        }
    }

    #endregion

    #region Protected Methods    

    protected bool TryRemoveDownload()
    {
        try
        {
            Debug.Log("Trying to remove download from " + downloadURL + " ... ");

            RemoveDownload();

            return true;
        }
        catch (Exception e)
        {
            Debug.Log("Download removal failed because of " + e.Message);

            return false;
        }
    }

    protected void RemoveDownload()
    {
        if (File.Exists(destinationPath))
        {
            File.Delete(destinationPath);
        }

        string fullPath = Path.GetDirectoryName(destinationPath);
        if (Directory.GetFiles(fullPath, "*.*", SearchOption.AllDirectories).Length == 0)
        {
            Directory.Delete(fullPath);
        }

        Debug.Log("Successfully removed download from " + downloadURL + ".");

        state = DownloadState.Created;
    }

    #endregion

    #region Callbacks

    /// <summary>
    /// callback from webclient changing the progress
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void DownloadProgressChanged_WebClient(object sender, DownloadProgressChangedEventArgs e)
    {
        Debug.Log("Progress of Download " + downloadURL + " = " + " received: "+ e.BytesReceived + " / " +e.TotalBytesToReceive);
    }

    /// <summary>
    /// callback from webclient completing the download
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void DownloadFileCompleted_WebClient(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Error != null)
        {
            //case if WebCLient.DownloadFileAsync is failed

            Debug.LogError("Download::DownloadFileCompleted_WebClient ( webClient.DownloadFileCompleted ) - Download Url: " + downloadURL + " Destination Path: " + destinationPath + " Error: " + e.Error.Message);
        }
        else if (e.Cancelled)
        {
            //case if WebCLient.CancelAsync is called
            //Debug.Log("Download::DownloadFileCompleted_WebClient ( webClient.DownloadFileCompleted ) - Cancelled Download of: " + sourceURL);
        }
        else
        {
            state = DownloadState.Finished;

            Debug.Log("Download::DownloadFileCompleted_WebClient ( webClient.DownloadFileCompleted ) - Finished Download from: " + downloadURL);
        }

        //removed because of callback from webclient thread is not possible
        //OnDownloadCompleted(new DownloadCompletedEventArgs(this, state));
    }

    #endregion
}

public class Downloader : MonoBehaviour {

    public string downloadURL;
    public string destinationFolder;
    public string fileName;

    protected Download currentDownload;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region Public Methods

    public void StartDownloadAsync()
    {
        StartCoroutine(StartDownload());
    }

    #endregion

    #region Protected Methods

    protected IEnumerator StartDownload()
    {
        Debug.Log("Starting Download ...");

        if (currentDownload != null)
        {
            Debug.Log("There is an already existing download. Will be deleting it ...");

            currentDownload.StopDownload();
            yield return currentDownload.TryRemoveDownloadUntilSuccessful();
        }

        string downloadDestinationPath = Application.persistentDataPath + "/" + destinationFolder;
        Directory.CreateDirectory(downloadDestinationPath);

        currentDownload = new Download(downloadURL, downloadDestinationPath + "/" + fileName);
        StartCoroutine(currentDownload.StartDownloadAsync());
    }

    #endregion
}
