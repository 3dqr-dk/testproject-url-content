﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Downloader), true)]
public class DownloaderEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();

        if (GUILayout.Button("Download from URL"))
        {
            Downloader editorTarget = (Downloader)target;

            editorTarget.StartDownloadAsync();
        }
    }
}
